import json
import ovh
import sys

serviceName = sys.argv[1]
domain = sys.argv[2]

if len(sys.argv) < 2:
    print ("Usage: %s serviceName domain" % sys.argv[0])
    sys.exit(1)

client = ovh.Client()

result = client.delete('/dedicated/server/' + serviceName + '/secondaryDnsDomains/' + domain )
print json.dumps(result, indent=4)
