#!/usr/bin/env bash
# Simple DirectAdmin OVH sdns api wrapper 
#
#This script is a part of Spoofy's
#software licenced under The BSD 3-Clause License.
#
#General BSD license.
#
#Copyright (c) 2020, Sp00Fy
#spoofy@twojapomoc.it
#
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#*-------------------------------------*

user="$1"
domain="$2"
curl_insecure_flag="--insecure"
basedir=$(dirname "$0")

if [ -z "$1" ] || [ -z "$2" ]; then
	echo "Usage: $0 da_username da_domain"
	exit 1
fi

da_add_record() {
	ownercheck_record="$($python_bin get_ownercheck.py "$ovh_servicename" "$domain")"
	record_name="$(echo "$ownercheck_record" | jq -c '.subDomain' | cut -d '"' -f 2)"
	record_type="$(echo "$ownercheck_record" | jq -c '.fieldType' | cut -d '"' -f 2)"
	record_value="$(echo "$ownercheck_record" | jq -c '.fieldValue' | cut -d '"' -f 2)"
	echo "record_name=${record_name}&test&type=${record_type}&val=${record_value}"
	curl $curl_insecure_flag --request POST --user "${da_user}":"${da_password}" "${da_server}/CMD_API_DNS_ADMIN?domain=${domain}&action=add&type=${record_type}&name=${record_name}&value=${record_value}"
}

ovh_add_sdns() {
	$python_bin add_sdns.py "$ovh_servicename" "$domain" "$ip"
}

source_confs() {
	domain_conf="/usr/local/directadmin/data/users/$user/domains/$domain.conf"
	ovh_conf="$basedir/ovh.conf"
	da_conf="$basedir/da.conf"
	if [ -e "$domain_conf" ]; then
		source "$domain_conf"
	else
		echo "Error! Domain configuration file not found! Aborting."
		exit 1
	fi

	if [ ! -e "$ovh_conf" ]; then
		echo "Error! Ovh api configuration file not found! Aborting."
		exit 1
	fi

	if [ -e "$da_conf" ]; then
		source "$da_conf"
	else
		echo "Error! Directadmin configuration file not found! Aborting."
		exit 1
	fi
}

source_confs
ovh_add_sdns
da_add_record
ovh_add_sdns
