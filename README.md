# da_ovh-sdns-wrapper

Simple DirectAdmin OVH slave DNS API wrapper

OVH API documentation: https://eu.api.ovh.com/
Python OVH API wrapper: https://github.com/ovh/python-ovh
