import json
import ovh
import sys

serviceName = sys.argv[1]

if len(sys.argv) < 1:
    print ("Usage: %s ip" % sys.argv[0])
    sys.exit(1)

client = ovh.Client()
result = client.get('/dedicated/server/' + serviceName + '/secondaryDnsDomains/')

print json.dumps(result, indent=4)
