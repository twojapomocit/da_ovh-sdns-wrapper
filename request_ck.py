# -*- encoding: utf-8 -*-

try:
    input = raw_input
except NameError:
    pass

import ovh
import sys

serviceName = sys.argv[1]

# create a client using configuration
client = ovh.Client()

# Request  API access
ck = client.new_consumer_key_request()
ck.add_rules(ovh.API_READ_WRITE, "/dedicated/server/" + serviceName + "/secondaryDnsDomains/")
ck.add_rules(ovh.API_READ_WRITE, "/dedicated/server/" + serviceName + "/secondaryDnsDomains/*")
ck.add_rules(ovh.API_READ_WRITE, "/dedicated/server/" + serviceName + "/secondaryDnsNameDomainToken/")

# Request token
validation = ck.request()

print("Please visit %s to authenticate" % validation['validationUrl'])
input("and press Enter to continue...")

# Print nice welcome message
print("Btw, your 'consumerKey' is '%s'" % validation['consumerKey'])
