import json
import ovh
import sys

serviceName = sys.argv[1]
domain = sys.argv[2]
ip = sys.argv[3]

if len(sys.argv) < 3:
    print ("Usage: %s serviceName domain ip" % sys.argv[0])
    sys.exit(1)

client = ovh.Client()
result = client.post('/dedicated/server/' + serviceName + '/secondaryDnsDomains/', 
    domain= domain,
    ip= ip,
)

print json.dumps(result, indent=4)
